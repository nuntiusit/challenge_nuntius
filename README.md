# Challenge_Nuntius

You can test it online with:  https://replit.com/@lgabregu/OrdinaryRealSystemadministrator#main.rb
## Installation

This proyect requires ruby, you can check if you have it installed with:

```
ruby -v
```
In case you dont have it, on linux install it with 
```
sudo apt-get install ruby-full
```

or check https://rvm.io/rubies/installing to install a good ruby manager 

Then clone this repo, inside the repo open a terminal an run:

```
ruby challenge.rb
```

You will be prompt with instructions to use the program.

It is possible to use the sample input provided within the file or make a manual input of the data.

## Usage

One assumptions was that the imput could be splitted and required in parts, this is for the manual input.
Also the user can choose to use sample data to test directly.

## Problem Statement
You can see the challenge in: https://gist.github.com/safplatform/792314da6b54346594432f30d5868f36