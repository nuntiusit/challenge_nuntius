# Challenge

def check_numeric
  begin
    input = gets.strip.downcase
    is_numeric = !Float(input).nil? rescue false
    puts 'Please, insert a numeric value..' unless is_numeric
  end until is_numeric
  input
end

def generate_ticket(input_articles)
  total = 0
  sale_taxes = 0
  input_articles.each do |article|
    tax = round_up(article[:price].to_f * get_tax_percentage(article))
    line_tax = article[:quantity].to_i * tax
    final_price = ((article[:price].to_f + tax) * article[:quantity].to_i).round(2)
    sale_taxes += line_tax
    line = "#{article[:quantity]} #{article[:imported]} #{article[:description]} : #{final_price}"
    total += final_price
    puts line
  end
  puts "Sales Taxes: #{sale_taxes.round(2)}"
  puts "Total: #{total}"
end

def get_tax_percentage(item)
  tax = 0
  tax += 0.05 if item[:imported] == 'imported'
  tax += 0.1 unless is_essential(item)
  tax
end

def is_essential(item)
  # TODO: use inflections to singularize input so plurals can be removed
  food_type = ['chocolate', 'chocolates']
  book_type = ['book', 'books']
  medical_type = ['pill', 'pills']

  words = item[:description].split(' ')

  essential = false
  words.each do |word|
    essential = true if food_type.include? word
    essential = true if book_type.include? word
    essential = true if medical_type.include? word
  end
  essential
end

def round_up(value)
  (value * 20).ceil / 20.0
end

def manual_input
  input_articles = []

  new_scan = 'y'
  while new_scan == 'y'
    puts '--SCANNING NEW PRODUCT--'
    puts 'Insert Quantity'
    quantity = check_numeric
    puts "Type 'imported' if the product is imported..."
    imported = gets.strip.downcase == 'imported' ? 'imported' : ''
    puts 'Insert Description...'
    description = gets.strip.downcase
    puts 'Insert Price...'
    price = check_numeric
    input_articles << { quantity: quantity, imported: imported, description: description, price: price }
    puts 'Product SCANNED, do you wish to add another? y/n'
    new_scan = gets.strip.downcase
  end
  puts 'All products scanned. your input is:'
  input_articles.each do |article|
    puts article.to_s
  end
  input_articles
end

def test_input(option)
  case option
  when '1'
    input_articles_test_1 = [
      { quantity: '2', imported: '', description: 'book', price: '12.49' },
      { quantity: '1', imported: '', description: 'music cd', price: '14.99' },
      { quantity: '1', imported: '', description: 'chocolate bar', price: '0.85' }
    ]
  when '2'
    input_articles_test_2 = [
      { quantity: '1', imported: 'imported', description: 'box of chocolates', price: '10.00' },
      { quantity: '1', imported: 'imported', description: 'bottle of perfume', price: '47.50' }
    ]
  else
    input_articles_test_3 = [
      { quantity: '1', imported: 'imported', description: 'bottle of perfume', price: '27.99' },
      { quantity: '1', imported: '', description: 'bottle of perfume', price: '18.99' },
      { quantity: '1', imported: '', description: 'packet of headache pills', price: '9.75' },
      { quantity: '3', imported: 'imported', description: 'boxes of chocolates', price: '11.25' }
    ]
  end
end

puts 'Hi, challenge accepted!'
puts 'Insert name'
name = gets.strip
puts "Hello #{name} let's scan some products!"

begin
  puts 'Do you want to use a sample? (y/n):'
  sample = $stdin.gets.strip.downcase
end until %w(y n).include?(sample)

if sample == 'y'
  begin
    puts 'Type 1, 2 or 3 to select a test option.'
    option = $stdin.gets.strip.downcase
  end until %w(1 2 3).include?(option)
  input_articles = test_input(option)
else
  input_articles = manual_input
end

puts 'Your OUTPUT is:'
generate_ticket(input_articles)
